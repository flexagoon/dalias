import os
import sys
import shutil

import yaml
from xdg import xdg_config_home

class Manager:
    def __init__(self):
        # Generate a config path
        configPath = xdg_config_home() / "dAliasList.yml"

        # Create a config if it doesn't exist
        if not os.path.exists(configPath):
            shutil.copy("defaultConfig.yml", configPath)

        # Read a config
        with open(configPath, "r") as configFile:
            self.config = yaml.safe_load(configFile)

    @staticmethod
    def argparse(alias, config):
        aparams = config[alias]
        directory = aparams["directory"]
        if isinstance(directory, str):
            directory = os.path.expanduser(aparams["directory"])
        elif isinstance(directory, list):
            directory = [os.path.expanduser(cdir) for cdir in directory]
        else:
            print("Invalid directory name in alias " + alias)
            sys.exit()
        cmd = aparams["do"]
        return directory, cmd
