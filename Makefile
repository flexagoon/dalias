PREFIX ?= /usr/local
INSTALL ?= $(DESTDIR)$(PREFIX)

.PHONY: default install uninstall

default:
	@echo "Run 'make install' to install dalias"

install:
	@mkdir -p $(INSTALL)/bin/daliasLibs
	@cp -p dalias.py $(INSTALL)/bin/dalias
	@cp -p *Manager.py $(INSTALL)/bin/daliasLibs
	@chmod 755 $(INSTALL)/bin/dalias

uninstall:
	@rm -rf $(INSTALL)/bin/dalias*
