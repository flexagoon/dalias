# Dalias
A tool to create directory-specific aliases

# Installation
## Manual Installation
### Prerequisites
Install `python` and `git` with your package manager  
Example:
```
pacman -S python git
```

### Installing dependencies 
Install xdg and pyyaml with `pip`
```
pip install xdg pyyaml
```

### Installing dalias
1. Clone the git repo
```
git clone https://gitlab.com/flexagoon/dalias
```
2. cd into the repo directory
```
cd dalias
```
3. Install dalias
```
sudo make install
```

# Configuration
The configuration is located in `~/.config/dAliasList.yml`. After running dalias for the first time, a default config with an overview of the configuration syntax will be generated. You can also see the default config in this repository, in the `defaultConfig.yml` file.
