#!/usr/bin/env python3

import os

from daliasLibs import configManager
from daliasLibs import shellManager
from daliasLibs import fileManager

cm = configManager.Manager()
sm = shellManager.Manager()
fm = fileManager.Manager()

# Get a config
config = cm.config

# Clean up previous aliases
fm.purge()

# Set up aliases
for alias in config:
    directory, cmd = cm.argparse(alias, config)
    fm.add(alias, directory, cmd)
