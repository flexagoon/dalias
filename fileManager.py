import os
import stat

class Manager:
    def __init__(self):
        # Create ~/.local/bin/dalias if it doesn't exist
        self.binpath = os.path.expanduser("~/.local/bin/dalias/")
        if not os.path.isdir(self.binpath):
            os.makedirs(binpath)

    def purge(self):
        # Get list of current aliases
        aliases = os.listdir(self.binpath)

        # Remove all aliases
        for alias in aliases:
            os.remove(self.binpath+alias)

    def add(self, name, directory, cmd):
        # Get executable file path
        aliaspath = self.binpath + name

        # Generate the file text depending on the number of directories
        if isinstance(directory, str):
            bintext = "if [ $PWD == {directory} ]; then {cmd}; fi"
        elif isinstance(directory, list):
            directory = "|".join(directory)
            bintext = '[[ "|{directory}|" != *"|$PWD|"* ]] && exit\n{cmd}'

        # Write the function into the file
        with open(aliaspath, "w") as executable:
            executable.write(bintext.format(directory=directory, cmd=cmd))

        # chmod 777 the file
        os.chmod(aliaspath, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
