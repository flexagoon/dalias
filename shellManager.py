import os
import sys

class Manager:
    def __init__(self):
        # Check if ~/.local/bin/dalias is in PATH
        if not ".local/bin/dalias" in os.environ["PATH"]:
            print("You should add ~/.local/bin/dalias to your $PATH!")
            print("Please do that and try running dalias again.")
            sys.exit()
